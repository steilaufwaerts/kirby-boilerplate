<?php

return [
    // local <=> dev <=> live
    'environment' => 'local',
    'debug' => true,
    'cache' => false,
];
