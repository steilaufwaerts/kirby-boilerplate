<!doctype html>
<html lang="<?= $kirby?->language()?->code() ?? 'de' ?>">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="canonical" href="<?= $page->url() ?>"/>

  <title><?= $site->title() ?></title>

  <?php snippet('head/meta'); ?>

  <?= css('assets/css/style.css') ?>
  <?php if (file_exists($kirby->root('assets') . '/css/templates/' . $page->intendedTemplate()->name() . '.css')) : ?>
    <?= css('assets/css/templates/' . $page->intendedTemplate()->name() . '.css') ?>
  <?php endif; ?>

  <?= js(['assets/js/index.js'], ['defer' => true]) ?>
  <?php if (file_exists($kirby->root('assets') . '/js/templates/' . $page->intendedTemplate()->name() . '.js')) : ?>
    <?= js(['assets/js/templates/' . $page->intendedTemplate()->name() . '.js'], ['defer' => true]) ?>
  <?php endif; ?>
</head>

<body class="is--<?php echo $page->intendedTemplate(); ?>">
<?php snippet('header') ?>

<main>

