<?php

$pageTitle = $page->title() . ' – ' . $site->seo_title();
if (!$page->seo_title()->isNotEmpty() && !$site->seo_title()->isNotEmpty()) {
  $pageTitle = $page->title() . ' – ' . $site->title();
} elseif ($page->seo_title()->isNotEmpty()) {
  $pageTitle = $page->seo_title();
} elseif ($page->isHomePage() && $site->seo_title()->isNotEmpty()) {
  $pageTitle = $site->seo_title();
}

$firstString = $page->title();
$secondString = $site->og_sitename()->isNotEmpty() ? $site->seo_sitle() : $site->og_sitename();
$ogTitle = $firstString . ' - ' . $secondString;
if ($page->seoTitle()->isNotEmpty()) {
  $ogTitle = $page->seoTitle();
} elseif ($page->isHomePage()) {
  $ogTitle = $site->seoTitle();
}

$ogImageUrl = '';
if ($page->ogImage()->isNotEmpty() && !is_null($page->ogImage()->toFile())) {
  $ogImageUrl = $page->ogImage()->toFile()->crop(1500, 786)->url();
} else if ($site->ogImage()->isNotEmpty() && !is_null($site->ogImage()->toFile())) {
  $ogImageUrl = $site->ogImage()->toFile()->crop(1500, 786)->url();
}

?>

<meta name="description" content="<?= $page->seoDescription()->or($site->seoDescription()) ?>">
<meta name="author" content="<?= $site->og_sitename() ?>">
<meta name="robots" content="noindex, nofollow">

<title><?= $pageTitle ?></title>

<?php foreach ($kirby->languages() as $language) : ?>
  <link rel="alternate" hreflang="<?= strtolower(html($language->code())); ?>"
        href="<?= $page->url($language->code()); ?>"/>
<?php endforeach; ?>

<?php if (!is_null($kirby->defaultLanguage())) : ?>
  <link rel="alternate" hreflang="x-default" href="<?= $page->url($kirby->defaultLanguage()->code()); ?>"/>
<?php endif; ?>

<meta property="og:title" content="<?= $ogTitle ?>">
<meta property="og:description" content="<?= $page->seo_description()->or($site->seo_description()) ?>"/>
<meta property="og:site_name" content="<?= $site->og_sitename() ?>">
<meta property="og:url" content="<?= $page->url() ?>"/>
<meta property="og:image" content="<?= $ogImageUrl ?>"/>
<meta property="og:locale" content="<?= $kirby?->language()?->locale(LC_ALL) ?? 'de_DE' ?>">
<meta property="og:image:width" content="1500"/>
<meta property="og:image:height" content="786"/>
<meta property="og:type" content="Website">

<meta name="twitter:card" content="<?= $site->twitter_card_type()->or('summary') ?>">
<meta name="twitter:image" content="<?= $ogImageUrl ?>">
<meta name="twitter:creator" content="<?= $site->twitter_card_site() ?>"/>
<meta name="twitter:site" content="<?= $site->twitter_card_site() ?>">
<meta name="twitter:title" content="<?= $ogImageUrl ?>">
<meta name="twitter:description" content="<?= $page->seo_description()->or($site->seo_description()) ?>">
