'use strict';
import gulp from "gulp";
import gulpSass from "gulp-sass";
import * as sass from "sass";
import minifyCss from "gulp-clean-css";
import sourceMaps from "gulp-sourcemaps";
import autoprefixer from "gulp-autoprefixer";
import minify from "gulp-minify";
import rollup from "gulp-better-rollup";
import babel from "rollup-plugin-babel";
import resolve from "rollup-plugin-node-resolve";
import globals from "rollup-plugin-node-globals";

const compileSass = gulpSass(sass);

function mainJsBuilder() {
    return gulp.src('src/*.js')
        .pipe(sourceMaps.init())
        .pipe(rollup({
            plugins: [babel(), resolve(), globals()]
        }, 'iife'))
        .pipe(minify({
            ext: {
                src: '.js',
                min: '.js'
            },
            noSource: false
        }))
        .pipe(sourceMaps.write('.'))
        .pipe(gulp.dest('assets/js'))
}

function templateJsBuilder() {
    return gulp.src('src/js/templates/*.js')
        .pipe(sourceMaps.init())
        .pipe(rollup({
            plugins: [babel(), resolve(), globals()]
        }, 'iife'))
        .pipe(minify({
            ext: {
                src: '.js',
                min: '.js'
            },
            noSource: true
        }))
        .pipe(sourceMaps.write('.'))
        .pipe(gulp.dest('assets/js/templates'))
}

function mainCssBuilder() {
    return gulp.src('src/*.scss')
        .pipe(sourceMaps.init())
        .pipe(compileSass({
            includePaths: ['node_modules']
        }, null).on('error', compileSass.logError))
        .pipe(minifyCss())
        .pipe(
            autoprefixer({
                cascade: true
            })
        )
        .pipe(sourceMaps.write('.'))
        .pipe(gulp.dest('assets/css'))
}
function templateCssBuilder() {
    return gulp.src('src/scss/templates/*.scss')
        .pipe(sourceMaps.init())
        .pipe(compileSass({
            includePaths: ['node_modules']
        }, null).on('error', compileSass.logError))
        .pipe(minifyCss())
        .pipe(
            autoprefixer({
                cascade: true
            })
        )
        .pipe(sourceMaps.write('.'))
        .pipe(gulp.dest('assets/css/templates'))
}

export const watch = () => {
    gulp.watch('src/**/*.scss', mainCssBuilder);
    gulp.watch('src/**/*.scss', templateCssBuilder);
    gulp.watch('src/**/*.js', mainJsBuilder);
    gulp.watch('src/**/*.js', templateJsBuilder);
}

export const buildCss = gulp.parallel(mainCssBuilder, templateCssBuilder);
export const buildMainCss = mainCssBuilder;
export const buildTemplateCss = templateCssBuilder;

export const buildJs = gulp.parallel(mainJsBuilder, templateJsBuilder);
export const buildMainJs = mainJsBuilder;
export const buildTemplateJs = templateJsBuilder;

export default gulp.parallel(mainCssBuilder, templateCssBuilder, mainJsBuilder, templateJsBuilder);
