[<svg><g><g><path d="M1,26.09,5.1,20.32a13.49,13.49,0,0,0,9.54,3.94c2.64,0,4.25-1,4.25-2.46C18.89,17.72,2,21,2,9.49,2,4.48,6.22,0,13.87,0A18.2,18.2,0,0,1,26,4.16L21.8,9.67a14.07,14.07,0,0,0-8.73-3.09c-2.24,0-3.22.89-3.22,2.15,0,3.8,16.87,1,16.87,12.13,0,6-4.43,10-12.44,10C8.24,30.84,4,29,1,26.09Z"></path><path d="M35.76,30.3V7.16H27.39V.45h24.4V7.16H43.46V30.3Z"></path><path d="M55,30.3V.45H76.9V7.16H62.71v4.66H76.58v6.71H62.71v5.06H76.9V30.3Z"></path><path d="M80.93,30.3V.45h7.69V30.3Z"></path><path d="M93.55,30.3V.45h7.7V23.59h12V30.3Z"></path><path d="M23.45,64.88,22,60.59H10.2L8.73,64.88H0L11.23,35H20.9L32.18,64.88ZM16.07,42.64,12.26,53.87h7.61Z"></path><path d="M32.27,52.8V35H40.1v17.5c0,3.53,2.11,6.09,6.32,6.09s6.26-2.56,6.26-6.09V35h7.79V52.75c0,7.43-4.52,12.67-14,12.67S32.27,60.14,32.27,52.8Z"></path><path d="M65.4,64.88V35H87.28v6.71H73.09V46.4H87v6.71H73.09V64.88Z"></path><path d="M26,99.46,21.44,80.75,16.92,99.46H8.77L.22,69.61H8.86l4.52,19.92,5.1-19.92h5.91L29.5,89.53,34,69.61h8.68L34.1,99.46Z"></path><path d="M57.58,99.46V95.17H47.69l-2.42,4.29H36.5L54.4,69.61H79.47v6.71H65.28V81H79.15v6.71H65.28v5.06H79.47v6.71Zm0-22.24L51.27,88.45h6.31Z"></path><path d="M100,99.46l-5-10H91v10h-7.7V69.61h15c6.62,0,10.38,4.39,10.38,9.94a9.05,9.05,0,0,1-6,9l6.09,11Zm.85-20c0-2-1.7-3.18-3.72-3.18H91v6.4H97.1C99.12,82.72,100.82,81.6,100.82,79.5Z"></path><path d="M118.68,99.46V76.32h-8.37V69.61H134.7v6.71h-8.32V99.46Z"></path><path d="M135.42,95.26l4.07-5.78A13.49,13.49,0,0,0,149,93.42c2.64,0,4.25-1,4.25-2.46,0-4.07-16.92-.81-16.92-12.31,0-5,4.25-9.49,11.9-9.49a18.2,18.2,0,0,1,12.13,4.16l-4.2,5.51a14.07,14.07,0,0,0-8.73-3.09c-2.24,0-3.22.9-3.22,2.15,0,3.81,16.87,1,16.87,12.13,0,6-4.43,10-12.44,10C142.63,100,138.42,98.12,135.42,95.26Z"></path></g></g></svg>](https://steilaufwaerts.de)
## STEILAUFWAERTS BOILERPLATE

To start developing and install dependencies you need<br>
- [Composer](https://getcomposer.org/)<br>
- [Node.js](https://nodejs.org/en/)<br>
- [npm](https://www.npmjs.com/)

Run ```composer boil``` to install composer/npm dependencies.

Run ```composer dev``` to update composer/npm dependencies. (better use only in development, use boil command if you dont know what you are doing)

Run ```composer start``` to get your small php interpreter running. The project will be available
at http://localhost:8000 and logging will be active in the executing terminal

To Update Kirby use ```composer update getkirby/cms``` but don't forget the versioning in composer.json.


### Styles and Scripts
style.scss and index.js must be compiled into /assets. Assuming that compiled files are ready
for live the files don't need .min. endings. You only need style.scss and index.js in your assets folder.

You can use template base .js and .css files. They live in the /assets/templates folder and need the same name as the template.
Remember, they are loaded right after the main style.scss and main inde.js.

-----

## Based on [Kirby Plainkit](https://github.com/getkirby/plainkit)

**For mor information visit [getkirby.com](https://getkirby.com).**

### Try Kirby for free
Kirby is not free software. However, you can try Kirby and the Starterkit on your local machine or on a test server as long as you need to make sure it is the right tool for your next project. … and when you’re convinced, [buy your license](https://getkirby.com/buy).

## What's Kirby?
- **[getkirby.com](https://getkirby.com)** – Get to know the CMS.
- **[Try it](https://getkirby.com/try)** – Take a test ride with our online demo. Or download one of our kits to get started.
- **[Documentation](https://getkirby.com/docs/guide)** – Read the official guide, reference and cookbook recipes.
- **[Issues](https://github.com/getkirby/kirby/issues)** – Report bugs and other problems.
- **[Feedback](https://feedback.getkirby.com)** – You have an idea for Kirby? Share it.
- **[Forum](https://forum.getkirby.com)** – Whenever you get stuck, don't hesitate to reach out for questions and support.
- **[Discord](https://chat.getkirby.com)** – Hang out and meet the community.
- **[YouTube](https://youtube.com/kirbyCasts)** - Watch the latest video tutorials visually with Bastian.
- **[Mastodon](https://mastodon.social/@getkirby)** – Spread the word.
- **[Instagram](https://www.instagram.com/getkirby/)** – Share your creations: #madewithkirby.

---

© 2009-2023 Bastian Allgeier  
[getkirby.com](https://getkirby.com) · [License agreement](https://getkirby.com/license)

----
